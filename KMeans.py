import nltk
from textblob import TextBlob
import csv
import re
import os
import pandas as pd
import numpy as np
import string
import scipy

from sklearn.feature_extraction import DictVectorizer,text
from sklearn. cluster import KMeans
from types import *
from time import time
import matplotlib.pyplot as plt

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

#def k_means(normalize_array):

exclude = set(string.punctuation)
company = pd.read_csv('ciq_name_topics.csv', sep=',')
com_id = company["1"]
com_labels = company["2"]
labels = com_id
sample_size = 1000

def bench_k_means(estimator, name, data):
	t0 = time()
	estimator.fit(data)
	print('% 9s   %.2fs    %i   %.3f   %.3f   %.3f   %.3f   %.3f    %.3f'
		% (name, (time() - t0), estimator.inertia_,
		metrics.homogeneity_score(labels, estimator.labels_),
		metrics.completeness_score(labels, estimator.labels_),
		metrics.v_measure_score(labels, estimator.labels_),
		metrics.adjusted_rand_score(labels, estimator.labels_),
		metrics.adjusted_mutual_info_score(labels,  estimator.labels_),
		metrics.silhouette_score(data, estimator.labels_,
									metric='euclidean',
									sample_size=sample_size)))
	#f.write('% 9s' % 'init'
     # '    time  inertia    homo   compl  v-meas     ARI AMI  silhouette')
	#f.write("\n")
	#f.write(str(name))
	#f.write(str((time() - t0)))
	#f.write(str(estimator.inertia_))

if __name__ == "__main__":
	train_path = 'ciq_name_topics.csv'
	f = open('new_ciq_name_topics.csv','r')
	word_vector = []
	k_Matrix = []
	new_row = []
	n_samples = 0
		#print sen
	for line in f.readlines():
		#print cell
		#arr.append([])
		Matrix = []
		order = 0
		n_samples += 1
		for cell in line.split(","):
			#print cell
			order += 1
			if order < 1:
				continue
			Matrix.append(float(cell))
		k_Matrix.append(np.array(Matrix))
	data = scale(k_Matrix) 
	n_features = len(Matrix)
	n_digits = 20
	print("n_digits: %d, \t n_samples %d, \t n_features %d"
		% (n_digits, n_samples, n_features))

	print(79 * '_')
	print('% 9s' % 'init'
      '    time  inertia    homo   compl  v-meas     ARI AMI  silhouette')
	f = open("KMeans_Result_.txt",'w')
	#bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10),
	#			name="k-means++", data=data)

	#bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10),
	#			name="random", data=data)

# in this case the seeding of the centers is deterministic, hence we run the
# kmeans algorithm only once with n_init=1
	pca = PCA(n_components=n_digits).fit(data)
	#bench_k_means(KMeans(init=pca.components_, n_clusters=n_digits, n_init=1),name="PCA-based",data=data)
	f.close()
	print(79 * '_')
###############################################################################
# Visualize the results on PCA-reduced data
	reduced_data = PCA(n_components=2).fit_transform(data)
	#print reduced_data[:1]
	#print len(reduced_data)
	kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
	bench_k_means(kmeans,name="PCA-based",data=reduced_data)
	bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10),
				name="k-means++", data=reduced_data)
	bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10),
				name="random", data=reduced_data)
	kmeans.fit(reduced_data)
	classified_data = kmeans.labels_

	s = open("cluster.csv","w")
	s.write("id")
	s.write(",")
	s.write("cluster number")
	s.write(",")
	s.write("\n")
	ComID = []
	for i in com_id:
		ComID.append(i)
	for cluster in set(classified_data):
		for i in range(len(classified_data)):
			if classified_data[i] == cluster:
				s.write(str(com_id[i]))
				s.write(",")
				#s.write(str(com_labels[i]))
				#s.write(",")
				s.write(str(classified_data[i]))
				s.write("\n")
	s.close()
# Step size of the mesh. Decrease to increase the quality of the VQ.
	h = .02     # point in the mesh [x_min, m_max]x[y_min, y_max].

# Plot the decision boundary. For that, we will assign a color to each
	x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
	y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
	xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
# Obtain labels for each point in mesh. Use last trained model.
	Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
# Put the result into a color plot
	Z = Z.reshape(xx.shape)
	plt.figure(1)
	plt.clf()
	plt.imshow(Z, interpolation='nearest',
			extent=(xx.min(), xx.max(), yy.min(), yy.max()),
			cmap=plt.cm.Paired,
			aspect='auto', origin='lower')

	plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
# Plot the centroids as a white X
	centroids = kmeans.cluster_centers_
	plt.scatter(centroids[:, 0], centroids[:, 1],
			marker="x", s=169, linewidths=3,
			color='w', zorder=10)
	plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
          'Centroids are marked with white cross')
	plt.xlim(x_min, x_max)
	plt.ylim(y_min, y_max)
	plt.xticks(())
	plt.yticks(())
	plt.show()
